package com.plivo.Utils;

public interface PlivoConstants {

   public String NUMBER = "/Number/";

   public String MESSAGE = "/Message/";
   public String PRICING = "/Pricing/";


   public static final String DEFAULT_URL = "https://api.plivo.com";

   public static final String BASE_END_POINT = "/v1/Account/";

   public  final String STAGING_CONFIG_FILE_PATH = System.getProperty("user.dir")+"/resources/config.properties";

   public  final String PROD_CONFIG_FILE_PATH = System.getProperty("user.dir")+"/resources/production.properties";

}
