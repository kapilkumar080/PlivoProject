package com.plivo.Utils;

import com.plivo.Base.Base;

import java.io.FileInputStream;
import java.util.Properties;

public class ConfigUtil {

    public static Properties CONFIG = null;
    private static ConfigUtil ConfigUtil = new ConfigUtil();
    String environmentName;

    private ConfigUtil() {
        if (CONFIG == null) {

            CONFIG = new Properties();
             environmentName = System.getProperty("environment");
            if (environmentName.equals("stage")) {
                try {

                    FileInputStream fs = new FileInputStream(PlivoConstants.STAGING_CONFIG_FILE_PATH);
                    CONFIG.load(fs);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (environmentName.equals("prod")) {
            try {

                FileInputStream fs = new FileInputStream(PlivoConstants.PROD_CONFIG_FILE_PATH);
                CONFIG.load(fs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    public static String getDataFromConfig(String key)
    {
        try{
            return CONFIG.getProperty(key);}
        catch(Exception e){
            LoggerUtils.error("Exception in get data from Config "+e.getMessage());
            return null;
        }
    }

}





